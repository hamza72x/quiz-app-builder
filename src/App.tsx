import React from "react";
import "./css/App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import QuizList from "./components/QuizList";
import AdminPage from "./pages/AdminPage";
import { Container } from "react-bootstrap";
import CreateEditQuizPage from "./pages/CreateEditQuizPage";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faUser, faEdit, faGripLines, faEye } from "@fortawesome/free-solid-svg-icons";
import ViewQuiz from "./pages/ViewQuiz";

library.add(faUser, faEdit, faGripLines, faEye);

export default function App(): React.ReactElement {
  return (
    <Router>
      <Container className="pt-3 pb-3">
        <Switch>
          <Route exact path="/">
            <QuizList isAdminPage={false} />
          </Route>
          <Route exact path="/admin">
            <AdminPage />
          </Route>
          <Route exact path="/admin/create">
            <CreateEditQuizPage isEditPage={false} />
          </Route>
          <Route exact path="/admin/edit/:quizId">
            <CreateEditQuizPage isEditPage={true} />
          </Route>
          <Route exact path="/view/:quizId">
            <ViewQuiz />
          </Route>
        </Switch>
      </Container>
    </Router>
  );
}
