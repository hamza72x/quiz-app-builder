import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState, useEffect } from "react";
import { Button, ListGroup } from "react-bootstrap";
import { QuizListProps } from "../models/Props";
import { Link } from "react-router-dom";
import { QuizType } from "../models/Types";
import QuizStorageInstance from "../utils/QuizStorage";

const QuizList = (props: QuizListProps): React.ReactElement => {
  const [quizes, setQuizes] = useState<QuizType[]>([]);

  // initialization
  useEffect(() => {
    loadDataFromStorage();
  }, []);

  const loadDataFromStorage = () => {
    setQuizes(QuizStorageInstance.quizes);
  }

  return (
    <div>
      <ListGroup>
        {quizes && quizes.map((e, i) => (
          <ListGroup.Item key={i}>
            {props.isAdminPage && (
              <a href="#" className="float-start grey me-2">
                <FontAwesomeIcon icon="grip-lines" />
              </a>
            )}
            <span>{e.title}</span>
            {props.isAdminPage && (
              <Link to={`/admin/edit/${e.id}`} className="float-end grey">
                <FontAwesomeIcon icon="edit" />
              </Link>
            )}
            <Link to={`/view/${e.id}`} className="float-end grey me-2">
              <FontAwesomeIcon icon="eye" />
            </Link>
          </ListGroup.Item>
        ))}
      </ListGroup>
      {props.isAdminPage && quizes.length === 0 && (
        <Button variant="primary"
          onClick={() => { QuizStorageInstance.insertDemoData(); loadDataFromStorage() }}>
          Insert Demo Data
        </Button>
      )}
    </div>
  );
};

export default QuizList;
