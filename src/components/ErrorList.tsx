import React from 'react';
import { Alert } from 'react-bootstrap';
import { ErrorListProps } from '../models/Props';

export default function ErrorList(props: ErrorListProps): React.ReactElement {

    const { errors } = props;

    if (errors && errors.length > 0) {
        return (
            <Alert variant="danger">
                <ul className="mb-0">
                    {errors.map((e, i) => (
                        <li key={i} >
                            {e}
                        </li>
                    ))}
                </ul>
            </Alert>
        )
    }

    return (<></>)
}