import React, { useEffect, useState } from 'react';
import { Button, Form, FormControl, FormGroup, InputGroup, Row, Table } from 'react-bootstrap';
import { createImportSpecifier } from 'typescript';
import { QuizQuestionType } from '../models/Types';

export default function QuestionForm(
    props: {
        question: QuizQuestionType;
        onChange: (question: QuizQuestionType) => void;
    }
): React.ReactElement {

    const [question, setQuestion] = useState<QuizQuestionType>(props.question);
    
    const handleAddNewOptionButton = () => {
        let newQuestion = { ...question, answers: [...question.answers, { isValidAnswer: false, answer: "", image: "", points: 0 }] };
        props.onChange(newQuestion);
        setQuestion(newQuestion)
    }

    return (
        <Row className="m-0 mt-3 br-10 p-2 pt-3 box-1">
            <h4>Question</h4>
            <FormGroup className="mb-3">
                <InputGroup>
                    <InputGroup.Text>Question {question.serial}</InputGroup.Text>
                    <FormControl
                        placeholder={`Ex: What's reverse shell?`}
                        type="text"
                        autoComplete="on"
                        value={question.question}
                        onChange={(e) => {
                            let newQuestion = { ...question, question: e.target.value };
                            console.log("there", e.target.value);
                            props.onChange(newQuestion);
                            setQuestion(newQuestion);
                        }}
                    ></FormControl>
                </InputGroup>
            </FormGroup>


            <FormGroup>
                <Table className="br-10" striped bordered hover>
                    <thead>
                        <tr>
                            <th>Option and Image</th>
                            <th>Right?</th>
                        </tr>
                    </thead>
                    <tbody>
                        {question.answers.map((ans, i) => {
                            return (
                                <tr key={`tr-${question.serial}-${i}`}>
                                    <td >
                                        <InputGroup className="pt-2">

                                            <InputGroup.Text>Option {i + 1}</InputGroup.Text>

                                            <FormControl
                                                type="text"
                                                autoComplete="on"
                                                value={ans.answer}
                                                placeholder={`Option ${i + 1}`} onChange={(e) => {

                                                    let newAnswers = question.answers
                                                    newAnswers[i] = { ...ans, answer: e.target.value }
                                                    let newQuestion = { ...question, answers: newAnswers };

                                                    props.onChange(newQuestion);
                                                    setQuestion(newQuestion);
                                                }} />
                                        </InputGroup>
                                        <InputGroup className="pt-2">

                                            <InputGroup.Text>Image URL {i + 1}</InputGroup.Text>

                                            <FormControl
                                                type="text"
                                                autoComplete="on"
                                                value={ans.image}
                                                placeholder={`Image URL ${i + 1}`} onChange={(e) => {

                                                    let newAnswers = question.answers
                                                    newAnswers[i] = { ...ans, image: e.target.value }
                                                    let newQuestion = { ...question, answers: newAnswers };

                                                    props.onChange(newQuestion);
                                                    setQuestion(newQuestion);
                                                }} />
                                        </InputGroup>
                                        <InputGroup className="pt-2">

                                            <InputGroup.Text>Points</InputGroup.Text>

                                            <FormControl
                                                placeholder={`Points`}
                                                type="number"
                                                className=""
                                                autoComplete="on"
                                                value={ans.points}
                                                onChange={(e) => {
                                                    let points = e.target.value === "" ? 0 : parseInt(e.target.value);
                                                    let newAnswers = question.answers
                                                    newAnswers[i] = { ...ans, points: points }
                                                    let newQuestion = { ...question, answers: newAnswers };

                                                    if (points !== 0) {
                                                        props.onChange(newQuestion);
                                                    }
                                                    setQuestion(newQuestion);
                                                }}
                                            ></FormControl>
                                        </InputGroup>

                                    </td>
                                    <td>

                                        <div key={`inline-checkbox-${question.serial}-${i}}`} className="mb-3">
                                            <Form.Check
                                                inline
                                                name={`group-${question.serial}-${i}`}
                                                type="checkbox"
                                                checked={ans.isValidAnswer}
                                                onChange={(e) => {

                                                    let newAnswers = question.answers
                                                    newAnswers[i] = { ...ans, isValidAnswer: ans.isValidAnswer !== true }
                                                    let newQuestion = { ...question, answers: newAnswers };

                                                    props.onChange(newQuestion);
                                                    setQuestion(newQuestion);
                                                }}
                                                id={`inline-checkbox-${question.serial}-${i}`}
                                            />
                                        </div>

                                    </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </Table>
            </FormGroup>
            <FormGroup>
                <Button
                    onClick={handleAddNewOptionButton}
                    className="mt-3 br-10"
                    variant="light"
                >
                    + Add new option
                </Button>
            </FormGroup>
        </Row>
    );
}