import { QuizQuestionType, QuizType } from "../models/Types";

const CACHE_KEY = "quizes";

class QuizStorage {
  quizes: QuizType[];

  constructor() {
    this.quizes = [];
    this.load();
  }

  insert(
    title: string,
    isLayoutSingle: boolean,
    questions: QuizQuestionType[]
  ) {
    // uuid should be used instead of integer
    let nextId = this.quizes.length + 1;

    this.quizes.push({
      id: nextId,
      isLayoutSingle: isLayoutSingle,
      questions: questions,
      title: title,
    });

    localStorage.setItem(CACHE_KEY, JSON.stringify(this.quizes));

    console.log("inserted", JSON.stringify(this.quizes));
  }

  update(
    id: number,
    title: string,
    isLayoutSingle: boolean,
    questions: QuizQuestionType[]
  ) {
    let index = this.quizes.findIndex((e) => e.id === id);

    if (index !== undefined) {
      console.log("Found quiz id, index:", index, "- id:", id);
      this.quizes[index].title = title;
      this.quizes[index].questions = questions;
      this.quizes[index].isLayoutSingle = isLayoutSingle;
    }

    localStorage.setItem(CACHE_KEY, JSON.stringify(this.quizes));

    console.log("updated", JSON.stringify(this.quizes));
  }

  get(id: number): QuizType | null {
    let filtered = this.quizes.filter((e) => e.id === id);

    if (filtered.length > 0) {
      return filtered[0];
    }
    return null;
  }

  load() {
    const data: string | null = localStorage.getItem(CACHE_KEY);

    if (data !== null && data !== undefined) {
      try {
        this.quizes = JSON.parse(data!);
        console.log("parsed", data);
      } catch (e) {
        console.log("error parsing localStorage.quizes to JSON", e);
        this.quizes = [];
      }
    } else {
      this.quizes = [];
    }
  }

  getQuestionsForUser(quizId: number): QuizQuestionType[] {
    const quiz = this.get(quizId);

    if (quiz === null) {
      return [];
    }

    return quiz.questions.map((question) => {
      return {
        ...question,
        answers: question.answers.map((answer) => {
          return {
            ...answer,
            isValidAnswer: false,
          };
        }),
      };
    });
  }

  isValidAnswer(
    quizId: number,
    questionIndex: number,
    answerIndex: number
  ): boolean {
    const quiz = this.get(quizId);

    if (quiz === null) {
      return false;
    }

    const question = quiz.questions[questionIndex];

    if (question === undefined) {
      return false;
    }

    const answer = question.answers[answerIndex];

    if (answer === undefined) {
      return false;
    }

    return answer.isValidAnswer;
  }

  insertDemoData() {
    console.log("inserting demo data");

    this.insert("Weekly Quiz", true, [
      {
        serial: 1,
        question: "What is the capital of the United States?",
        answers: [
          { answer: "Washington", isValidAnswer: true, image: "", points: 10 },
          { answer: "Los Angles", isValidAnswer: false, image: "", points: 0 },
        ],
      },
      {
        serial: 2,
        question: "What is the best cute animal?",
        answers: [
          { answer: "Cat", isValidAnswer: true, image: "", points: 20 },
          { answer: "Dog", isValidAnswer: false, image: "", points: 0 },
          { answer: "Lion xD", isValidAnswer: false, image: "", points: 0 },
        ],
      },
    ]);

    this.load();
  }
}

const QuizStorageInstance = new QuizStorage();

export default QuizStorageInstance;
