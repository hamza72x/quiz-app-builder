import React, { useEffect, useState } from 'react';
import { Row, Button, Col, Table, Form } from 'react-bootstrap';
import { useParams } from 'react-router';
import { ParamEditQuizPageType } from '../models/Params';
import { QuizListProps } from '../models/Props';
import { QuizQuestionType, QuizType } from '../models/Types';
import QuizStorageInstance from '../utils/QuizStorage';

const SingleQuestionView = (
    props: {
        userQuestionIndex: number,
        userQuestion: QuizQuestionType,
        onUserAnswer: (userQuestionIndex: number, userQuestion: QuizQuestionType) => void
    }
): React.ReactElement => {

    const questionIndex = props.userQuestionIndex;
    const [userQuestion, setQuestion] = useState<QuizQuestionType>(props.userQuestion);

    return (
        <Row className="box-1 p-3 br-10 m-2">
            <h4>{userQuestion.question}</h4>

            <Table className="br-10" striped bordered hover>
                <thead>
                    <tr>
                        <th>Option</th>
                        <th>Is this Right?</th>
                    </tr>
                </thead>
                <tbody>
                    {userQuestion.answers.map((ans, i) => {
                        return (
                            <tr key={`tr-${questionIndex}-${i}`}>
                                <td >
                                    <p>{ans.answer}</p>
                                    <p>{ans.image && <img src={ans.image} className="br-10" alt="" width={350} height={200} />}</p>
                                </td>
                                <td>
                                    <div key={`inline-checkbox-${questionIndex}-${i}}`} className="mb-3">
                                        <Form.Check
                                            inline
                                            name={`group${questionIndex}${i}`}
                                            type="checkbox"
                                            checked={ans.isValidAnswer}
                                            onChange={(e) => {
                                                let newAnswers = userQuestion.answers
                                                newAnswers[i] = { ...ans, isValidAnswer: ans.isValidAnswer !== true }
                                                let newQuestion = { ...userQuestion, answers: newAnswers };
                                                props.onUserAnswer(questionIndex, newQuestion);
                                                setQuestion(newQuestion);
                                            }}
                                            id={`inline-checkbox-${questionIndex}${i}`}
                                        />
                                    </div>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </Table>
        </Row>
    )
}

export default function ViewQuiz(): React.ReactElement {

    const [isViewResult, setIsViewResult] = useState<boolean>(false);
    const [quiz, setQuiz] = useState<QuizType | null>();
    const [userQuestions, setUserQuestions] = useState<QuizQuestionType[]>([]);
    const [visibleUserQuestionIndex, setVisibleUserQuestionIndex] = useState<number>(0);
    const [isLoaded, setIsLoaded] = useState<boolean>(false);
    const [totalPoints, setTotalPoints] = useState<number>(0);

    const { quizId } = useParams<ParamEditQuizPageType>();

    // componentDidMount
    useEffect(() => {

        if (quizId !== null) {
            const quiz = QuizStorageInstance.get(parseInt(quizId!));
            if (quiz !== null) {
                setQuiz(quiz);
                setUserQuestions(QuizStorageInstance.getQuestionsForUser(quiz.id));
            }
        }

        setIsLoaded(true);
    }, []);

    const onUserAnswer = (questionIndex: number, updatedQuestion: QuizQuestionType) => {

        let newUserQuestions = userQuestions;
        newUserQuestions[questionIndex] = updatedQuestion;
        setUserQuestions(newUserQuestions);

        let newTotalPoints = 0;

        newUserQuestions.forEach((question, qi) => {
            question.answers.forEach((answer, ai) => {
                // first answer.isValidAnswer determines that user selected that field
                if (answer.isValidAnswer && QuizStorageInstance.isValidAnswer(quiz!.id, qi, ai)) {
                    newTotalPoints += answer.points;
                }
            })
        })

        setTotalPoints(newTotalPoints);
    }

    const handleCheckResult = () => {
        setIsViewResult(true);
    }

    const handleReset = () => {
        setUserQuestions(QuizStorageInstance.getQuestionsForUser(quiz!.id));
        setIsViewResult(false);
    }

    const handleNext = () => {
        if (visibleUserQuestionIndex + 1 < userQuestions.length) {
            setVisibleUserQuestionIndex(visibleUserQuestionIndex + 1);
        }
    }

    const handlePrev = () => {
        if (visibleUserQuestionIndex > 0) {
            setVisibleUserQuestionIndex(visibleUserQuestionIndex - 1);
        }
    }

    if (!isLoaded) {
        return <></>;
    }

    if (quiz === null) {
        return <h2>Quiz was not found!</h2>
    }

    if (isViewResult) {
        return (
            <Row>
                <h2>Your score: {totalPoints}</h2>
                <Col>
                    <Button onClick={handleReset}>Reset</Button>
                </Col>
            </Row>
        )
    }
    return (
        <>
            <h2>{quiz!.title}</h2>
            <Row className="mt-3 mb-3">
                <Col className="">

                    <Button onClick={handleCheckResult} className="ms-1 me-1" variant="success">Check Result</Button>

                    {
                        quiz!.isLayoutSingle
                        && visibleUserQuestionIndex !== 0
                        && <Button className="float-end" onClick={handlePrev} variant="light">Previous Question</Button>
                    }
                    {
                        quiz!.isLayoutSingle
                        && visibleUserQuestionIndex + 1 !== userQuestions.length
                        && <Button className="float-end" onClick={handleNext} variant="light">Next Question</Button>
                    }
                </Col>
            </Row>

            <Row className="m-0">

                {!quiz!.isLayoutSingle && userQuestions.map((q, i) => <SingleQuestionView
                    key={i}
                    userQuestion={q}
                    userQuestionIndex={i}
                    onUserAnswer={onUserAnswer} />
                )}

                {quiz!.isLayoutSingle && <SingleQuestionView
                    key={visibleUserQuestionIndex}
                    userQuestion={userQuestions[visibleUserQuestionIndex]}
                    userQuestionIndex={visibleUserQuestionIndex}
                    onUserAnswer={onUserAnswer} />
                }
            </Row>

        </>
    )
}