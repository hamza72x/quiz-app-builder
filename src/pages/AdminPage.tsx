import React from 'react';
import { InputGroup, FormControl, Button, Col, Row, Container } from 'react-bootstrap';
import QuizList from '../components/QuizList';
import { Link } from 'react-router-dom';

export default function AdminPage() {
    return (
        <>
            <Row>
                <InputGroup className="mb-3">
                    <Col className="me-2">
                        <FormControl className="br-10" placeholder="Filter">
                        </FormControl>
                    </Col>
                    <Link to="/admin/create">
                        <Button variant="success" className="br-10">
                            + Add New
                        </Button>
                    </Link>
                </InputGroup>
            </Row>
            <QuizList isAdminPage={true}></QuizList>
        </>
    );
}
