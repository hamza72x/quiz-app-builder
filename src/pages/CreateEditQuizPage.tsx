import React, { useEffect, useState } from "react";
import {
  Form,
  FormControl,
  InputGroup,
  Button,
  FormGroup,
} from "react-bootstrap";

import { useHistory, useParams, Link } from "react-router-dom";
import ErrorList from "../components/ErrorList";
import QuestionForm from "../components/QuestionForm";
import { ParamEditQuizPageType } from "../models/Params";
import { CreateEditQuizPageProps } from "../models/Props";
import { QuizQuestionType, QuizType } from "../models/Types";
import QuizStorageInstance from "../utils/QuizStorage";

function CreateEditQuizPage(props: CreateEditQuizPageProps): React.ReactElement {

  const history = useHistory();

  const { isEditPage } = props;
  const { quizId } = useParams<ParamEditQuizPageType>();

  // states
  const [errors, setErrors] = useState<string[]>([]);
  const [title, setTitle] = useState<string>("");
  const [isLayoutSingle, setIsLayoutSingle] = useState<boolean>(false);

  const [questions, setQuestions] = useState<QuizQuestionType[]>([]);

  const [editingQuiz, setEditingQuiz] = useState<QuizType | null>();

  const [isLoaded, setIsLoaded] = useState<boolean>(false);

  // init
  useEffect(() => {

    if (isEditPage) {

      let quiz = QuizStorageInstance.get(parseInt(quizId!));

      if (quiz !== null) {
        setTitle(quiz!.title);
        setQuestions(quiz!.questions);
        setIsLayoutSingle(quiz!.isLayoutSingle);
      }

      setEditingQuiz(quiz);

    } else {
      setQuestions([{
        serial: 1,
        question: "",
        answers: [
          { answer: "", isValidAnswer: false, image: "", points: 0 },
          { answer: "", isValidAnswer: false, image: "", points: 0 }
        ]
      }]);
    }

    setIsLoaded(true);

  }, []);

  // methods
  const onQuestionChange = (question: QuizQuestionType) => {

    let index = questions.findIndex(q => q.serial === question.serial);
    if (index === -1) {
      return;
    }

    let newQuestions = questions;
    newQuestions[index] = question;
    setQuestions(newQuestions);

  };

  // handleAddNewOptionButton
  const handleAddNewQuestionButton = () => {
    setQuestions([...questions, {
      serial: questions.length + 1,
      question: "",
      answers: [
        { answer: "", isValidAnswer: false, image: "", points: 0 },
        { answer: "", isValidAnswer: false, image: "", points: 0 }
      ]
    }]);
  };

  // handleInsertButton
  const handleSubmitButton = () => {

    setErrors([]);

    let newErrors: string[] = [];
    let totalFilledQuestion = 0;
    let hasMinimum2Answer = true;

    if (title.trim().length <= 0) {
      newErrors.push("Please enter the Quiz title");
    }

    questions.forEach((question) => {

      let filledAnswer = 0;

      if (question.question.trim().length > 0) {
        totalFilledQuestion += 1;
      }

      question.answers.forEach((answer) => {
        if (answer.answer.trim().length > 0) {
          filledAnswer += 1;
        }
      })

      if (filledAnswer < 1) {
        hasMinimum2Answer = false;
      }
    });

    if (totalFilledQuestion === 0) {
      newErrors.push("Please provide at least 1 question!");
    }

    if (!hasMinimum2Answer) {
      newErrors.push("You have to provide minium 2 options for each question");
    }

    if (newErrors.length > 0) {
      setErrors(newErrors);
      window.scrollTo(0, 0);
    } else {
      // actual insert/update
      if (isEditPage && quizId) {
        QuizStorageInstance.update(parseInt(quizId!), title, isLayoutSingle, questions);
      } else {
        QuizStorageInstance.insert(title, isLayoutSingle, questions);
      }
      history.push("/admin");
    }
  };

  // a better loader would be good
  if (!isLoaded) {
    return <></>
  }

  if (isEditPage && editingQuiz == null) {
    return (
      <ErrorList errors={["The quiz was not found!"]} />
    )
  }

  return (
    <>
      <Link className="float-end" to="/admin">
        <Button variant="light">/admin</Button>
      </Link>
      <h4 className="pb-2">{isEditPage ? 'Update the' : 'Create a'} Quiz {isEditPage ? `- ${quizId}` : ''}</h4>
      <Form>
        <ErrorList errors={errors} />

        <InputGroup>
          <FormControl
            className="br-10"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
            placeholder="Quiz Title"
          ></FormControl>
        </InputGroup>

        {questions.map((e, i) => (
          <QuestionForm
            key={`question-form-${i}`}
            question={e}
            onChange={onQuestionChange}
          />
        ))}

        <FormGroup>
          <Button
            onClick={handleAddNewQuestionButton}
            className="mt-3 br-10"
            variant="light"
          >
            + Add new question
          </Button>
        </FormGroup>

        <FormGroup className="pt-3">
          <div key={`inline-checkbox-layout`} className="mb-3">
            <Form.Check
              inline
              name={`group-layout`}
              label="Show single question per page?"
              type="checkbox"
              checked={isLayoutSingle}
              onChange={(e) => {
                setIsLayoutSingle(!isLayoutSingle)
              }}
              id={`inline-checkbox-layout`}
            />
          </div>
        </FormGroup>

        <Button
          onClick={handleSubmitButton}
          className="mt-3 br-10"
          variant="success"
        >
          {isEditPage ? "Update" : "Insert"}
        </Button>
      </Form>
    </>
  );
}

export default CreateEditQuizPage;
