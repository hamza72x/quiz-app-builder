interface QuizType {
  id: number;
  title: string;
  isLayoutSingle: boolean;
  questions: QuizQuestionType[];
}

interface QuizQuestionType {
  serial: number;
  question: string;
  answers: QuizAnswerType[];
}

interface QuizAnswerType {
  answer: string;
  image: string;
  points: number;
  isValidAnswer: boolean;
}

export type { QuizType, QuizQuestionType };
