interface QuizListProps {
  isAdminPage: boolean;
}

interface ErrorListProps {
  errors: string[];
}

interface CreateEditQuizPageProps {
  isEditPage: boolean;
}

export type { QuizListProps, ErrorListProps, CreateEditQuizPageProps };
